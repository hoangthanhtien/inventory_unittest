from constant import Config
import requests, unittest
import json
from openpyxl import load_workbook


class TestItems(unittest.TestCase):
    # add items
    def test_add_items_exist(self):
        """
        Test thêm mới hàng hóa trùng tên 
        """
        payload = {
            'item_name':'Dừa xiêm bến tre loại 1',
            'unit_uid': 'da0cc896-18ff-48c9-bae3-a8aa84191eeb'
        }
        headers ={
            'user-token': Config.ACCESS_TOKEN
        }
        response = requests.post(f"{Config.IVT_DOMAIN}/api/v1/catalogs/items", json.dumps(payload),headers=headers)
        self.assertNotEqual(response.status_code, 200)

    def test_add_new_items(self):
        """
        Test thêm mới hàng hóa 
        """
        payload = {
            'item_name':'New Item 1',
            'unit_uid': 'da0cc896-18ff-48c9-bae3-a8aa84191eeb'
        }
        headers ={
            'user-token': Config.ACCESS_TOKEN
        }
        response = requests.post(f"{Config.IVT_DOMAIN}/api/v1/catalogs/items", json.dumps(payload),headers=headers)
        if response.status_code == 200:
            content = response.json()
            delete_item_byid(content['data']['id'])
        self.assertEqual(response.status_code, 200)

    def test_add_items_itemname_isnull(self):
        """
        Test thêm mới hàng hóa là chuỗi rỗng
        """
        payload = {
            'item_name':'',
            'unit_uid': 'da0cc896-18ff-48c9-bae3-a8aa84191eeb'
        }
        headers ={
            'user-token': Config.ACCESS_TOKEN
        }
        response = requests.post(f"{Config.IVT_DOMAIN}/api/v1/catalogs/items", json.dumps(payload),headers=headers)
        if response.status_code == 200:
            content = response.json()
            delete_item_byid(content['data']['id'])
        breakpoint()
        self.assertEqual(response.status_code, 400)

    def test_add_items_unituid_isnull(self):
        """
        Tạo mới hàng hóa mà không có đơn vị tính 
        """
        payload = {"item_name": "test add item", "unit_uid": ""}
        headers = {"user-token": Config.ACCESS_TOKEN}
        response = requests.post(
            f"{Config.IVT_DOMAIN}/api/v1/catalogs/items",
            json.dumps(payload),
            headers=headers,
        )
        self.assertEqual(response.status_code, 400)

    # get items
    def test_get_list_item(self):
        """
        Test lấy danh sách hàng hóa 
        """
        headers ={
            'user-token': Config.ACCESS_TOKEN
        }
        payload = {}
        response = requests.post(f"{Config.IVT_DOMAIN}/api/v1/catalogs/item_prices",json.dumps(payload),headers=headers)
        self.assertEqual(response.status_code, 200)

    def test_get_list_item_pagination(self):
        """
        Test lấy danh sách hàng hóa theo phần trang 
        """
        payload = {"page": 2, "item_class_uid": ""}
        headers = {"user-token": Config.ACCESS_TOKEN}
        response = requests.post(
            f"{Config.IVT_DOMAIN}/api/v1/catalogs/item_prices",
            json.dumps(payload),
            headers=headers,
        )
        self.assertEqual(response.status_code, 200)

    def test_get_list_item_by_itemclass(self):
        """
        Test lấy danh sách hàng hóa theo nhóm hàng  
        """
        payload = {"item_class_uid": "0630e497-9403-42d5-92f9-7136f5fe171a"}
        headers = {"user-token": Config.ACCESS_TOKEN}
        response = requests.post(
            f"{Config.IVT_DOMAIN}/api/v1/catalogs/item_prices",
            json.dumps(payload),
            headers=headers,
        )
        self.assertEqual(response.status_code, 200)

    def test_get_item_by_id(self):
        """
        Test lấy thông tin hàng hóa theo id 
        """
        params = {"item_uid": "8c036c35-4dcf-46a0-87cc-c286db2d2fe2"}
        headers = {"user-token": Config.ACCESS_TOKEN}
        response = requests.get(
            f"{Config.IVT_DOMAIN}/api/v1/catalogs/item_prices",
            params=params,
            headers=headers,
        )
        self.assertEqual(response.status_code, 200)

    # update item
    def test_update_item(self):
        """
        Test cập nhật thông tin hàng hóa
        """
        payload = {
            "id": "313d1a80-54ad-4905-9c50-d78fad37bc69",
            "created_at": 1603271961,
            "created_by": "admin@gonstack.com",
            "updated_at": 1603271961,
            "updated_by": "buyer@amg.com",
            "deleted": False,
            "item_id": "00000003",
            "item_name": "Giấy vệ sinh Calla Horeca 2 lớp 10 cuộn/ bịch",
            "type": 1,
            "description": "",
            "active": 1,
            "unit_uid": "6a1ea777-dcb1-4b8a-a49b-65104eab9ab4",
            "item_class_uid": "814bfdc7-0a38-4211-b080-b2789b74776d",
            "owner_uid": "1371cbb8-097f-4865-a1e9-10247cfa5a1f",
            "company_uid": "1371cbb8-097f-4865-a1e9-10247cfa5a1f",
            "prices": [],
            "pre_order_prices_15": [],
            "pre_order_prices_30": [],
            "pre_order_prices_45": [],
            "item_class": {
                "id": "814bfdc7-0a38-4211-b080-b2789b74776d",
                "created_at": 1601965960,
                "created_by": "admin@gonstack.com",
                "updated_at": 1601965960,
                "updated_by": "admin@gonstack.com",
                "deleted": False,
                "item_class_id": "63211",
                "item_class_name": "GIAY VE SINH",
                "active": 1,
                "extra_data": {},
                "owner_uid": "1371cbb8-097f-4865-a1e9-10247cfa5a1f",
                "company_uid": "1371cbb8-097f-4865-a1e9-10247cfa5a1f",
                "parent_item_class_uid": "f27a0874-ecd1-49ea-a27c-b3464f80a24e",
            },
            "unit": {
                "id": "6a1ea777-dcb1-4b8a-a49b-65104eab9ab4",
                "created_at": 1602233304,
                "created_by": "admin@gonstack.com",
                "updated_at": 1602233304,
                "updated_by": "admin@gonstack.com",
                "deleted": False,
                "unit_id": "BICH",
                "unit_name": "bịch",
                "extra_data": {},
                "active": 1,
                "owner_uid": "1371cbb8-097f-4865-a1e9-10247cfa5a1f",
                "company_uid": "1371cbb8-097f-4865-a1e9-10247cfa5a1f",
            },
            "barcodes": [
                {
                    "id": "77cf41d9-2404-4c82-bfc1-fe38cd49d3a1",
                    "barcode_id": "8938506886441",
                }
            ],
            "created_at_date": "2020/10/21",
            "suppliers_id": "",
            "suppliers_name": "",
            "sale_prices": [],
            "promotion_sale_prices": [],
            "base_sale_prices": [],
            "latest_prices": [],
            "barcode": "8938506886441",
            "item_status": "Khóa Đặt hàng, Nhập hàng",
            "list_item_uid": [],
            "lock_order": True,
            "lock_gr": True,
            "lock_gi": False,
            "lock_sale": False,
            "uid": "313d1a80-54ad-4905-9c50-d78fad37bc69",
        }
        headers = {"user-token": Config.ACCESS_TOKEN}
        response = requests.put(
            f"{Config.IVT_DOMAIN}/api/v1/catalogs/items",
            json.dumps(payload),
            headers=headers,
        )
        self.assertEqual(response.status_code, 200)

    # delete item
    def test_delete_item(self):
        """
            Test xóa hàng hóa theo id 
        """

        payload = {
            'item_name':'Delete Item',
            'unit_uid': 'da0cc896-18ff-48c9-bae3-a8aa84191eeb'
        }
        headers ={
            'user-token': Config.ACCESS_TOKEN
        }
        response = requests.post(f"{Config.IVT_DOMAIN}/api/v1/catalogs/items", json.dumps(payload),headers=headers)

        self.assertEqual(response.status_code, 200)
        item_uid = response.json().get("data").get('id')
        delete_response = delete_item_byid(item_uid)
        self.assertEqual(delete_response, True)


    def test_import_item_from_excel(self):
        """
            Test import hàng hóa theo file excel 
        """

        headers = {"user-token": Config.ACCESS_TOKEN}
        with open(
            "./asset/test_import_new_items.xlsx", "rb"
        ) as excel_file:
            response = requests.post(
                f"{Config.IVT_DOMAIN}/api/v1/catalogs/items",
                files={"excel": excel_file},
                headers=headers,
            )
            self.assertEqual(response.status_code, 200)

    def delete_all_items_in_excel_file(self):
        wb = load_workbook(filename = './asset/test_import_new_items.xlsx')
        ws = wb.active
        result = []
        for i, value in enumerate(ws['B']):
            if i > 1:
                result.append(ws[f"B{i}"].value)
        for item_name in result:
            url = f"{Config.IVT_DOMAIN}/api/v1/item"+'?q={filters:[{"item_name":{"$eq":"'+item_name+'"}}]}'
            response = requests.get(url)  
            print("response", response)
            print(url)
        print(result)


def checkExistItem(item_name: str = ""):
    """
    Kiểm tra đã có hàng hóa trùng tên tồn tại
    """
    headers = {"user-token": Config.ACCESS_TOKEN}
    response = requests.get(
        Config.IVT_DOMAIN + "/api/v1/catalogs/items?search=" + item_name,
        headers=headers,
    )
    items_num = len(response.json().get("data"))
    return True if items_num > 0 else False

def delete_item_byid(item_id: str = ""):
    headers ={
        'user-token': Config.ACCESS_TOKEN
    }
    payload = {
        "uid": item_id
    }
    response = requests.delete(f"{Config.IVT_DOMAIN}/api/v1/catalogs/items", data=json.dumps(payload),headers=headers)
    if response.status_code == 200:
        return True
    return False

def delete_all_items_in_excel_file():
    wb = load_workbook(filename = './asset/test_import_new_items.xlsx')
    ws = wb.active
    for i, value in enumerate(ws['B']):
        print(i, value)



# def test_get_item(self):
#     pass


# def test_get_item_failed(self):
#     pass


# def test_insert_item(self):
#     pass


# def test_insert_item_failed(self):
#     pass


# def test_update_item(self):
#     pass


# def test_update_item_failed(self):
#     pass


# def test_delete_item(self):
#     pass


# def test_delete_item_failed(self):
#     pass


# def test_item_upload_excel(self):
#     pass


# def test_item_upload_excel_failed(self):
#     pass

