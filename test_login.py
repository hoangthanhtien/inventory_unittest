import requests 
import unittest 
from constant import Config 
import json

class TestLogin(unittest.TestCase):
    def test_login(self):
        """
            Test login với tài khoản đã được đăng ký
        """
        payload = {
            "user_email" : Config.TEST_ACCOUNT,
            "user_password" : Config.TEST_ACCOUNT_PASSWORD
        } 
        response = requests.post(f"{Config.IVT_DOMAIN}/api/v1/users/login", json.dumps(payload))
        
        self.assertEqual(response.status_code, 200)
        Config.ACCESS_TOKEN = response.json().get('data').get('user_token')

    def test_login_failed(self):
        """
            Test login thất bại với tài khoản chưa được đăng ký  
        """
        payload = {
            "user_email" : "dummy_account@gmail.com",
            "user_password" : "123456" 
        } 
        response = requests.post(f"{Config.IVT_DOMAIN}/api/v1/users/login", json.dumps(payload))
        self.assertEqual(response.status_code, 401)

    def test_invalid_payload(self):
        """
            Test login với tài khoản không hợp lệ  
        """
        payload = {
            "user_email" : "this_is_not_an_email",
            "user_password" : "12" 
        } 
        response = requests.post(f"{Config.IVT_DOMAIN}/api/v1/users/login", json.dumps(payload))
        self.assertEqual(response.status_code, 401)
