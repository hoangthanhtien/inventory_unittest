import requests 
import unittest 
from constant import Config 
import time

class TestPurchaseOrder(unittest.TestCase):
    def test_get_all_pending_purchase_orders(self):
        """
            Test lấy tất cả thông tin các đơn đặt hàng đang chờ duyệt
        """
        headers = {
            'user-token' : Config.ACCESS_TOKEN
        }
        current_timestamp = int(time.time())
        response = requests.get(f"{Config.IVT_DOMAIN}/api/v1/services/purchase-order?status=3&types=external&from_date=1632848400&to_date={current_timestamp}&results_per_page=10000&warehouses_uid=164902bd-8c23-4ae9-b030-15c305795bb5", headers=headers)
        self.assertEqual(response.status_code, 200)


    def test_get_all_success_purchase_orders(self):
        """
            Test lấy tất cả thông tin các đơn đặt hàng đang chờ duyệt
        """
        headers = {
            'user-token' : Config.ACCESS_TOKEN
        }
        current_timestamp = int(time.time())
        response = requests.get(f"{Config.IVT_DOMAIN}/api/v1/services/purchase-order?status=1&types=external&from_date=1632848400&to_date={current_timestamp}&results_per_page=10000&warehouses_uid=164902bd-8c23-4ae9-b030-15c305795bb5", headers=headers)
        self.assertEqual(response.status_code, 200)


    def test_get_all_accepted_purchase_orders(self):
        """
            Test lấy tất cả thông tin các đơn đặt hàng đang chờ duyệt
        """
        headers = {
            'user-token' : Config.ACCESS_TOKEN
        }
        current_timestamp = int(time.time())
        response = requests.get(f"{Config.IVT_DOMAIN}/api/v1/services/purchase-order?status=5&types=external&from_date=1632848400&to_date={current_timestamp}&results_per_page=10000&warehouses_uid=164902bd-8c23-4ae9-b030-15c305795bb5", headers=headers)
        self.assertEqual(response.status_code, 200)


    def test_get_all_canceled_purchase_orders(self):
        """
            Test lấy tất cả thông tin các đơn đặt hàng đã hủy 
        """
        headers = {
            'user-token' : Config.ACCESS_TOKEN
        }
        current_timestamp = int(time.time())
        response = requests.get(f"{Config.IVT_DOMAIN}/api/v1/services/purchase-order?status=2&types=external&from_date=1632848400&to_date={current_timestamp}&results_per_page=10000&warehouses_uid=164902bd-8c23-4ae9-b030-15c305795bb5", headers=headers)
        self.assertEqual(response.status_code, 200)


    def test_search_all_purchase_orders_by_item_id(self):
        """
            Test tìm kiếm tất cả đơn đặt hàng theo mã hàng 
        """
        headers = {
            'user-token' : Config.ACCESS_TOKEN
        }
        response = requests.get(f"{Config.IVT_DOMAIN}/api/v1/services/purchase-order?item_id=00000088&results_per_page=10000&warehouses_uid=297a8e2d-e5f0-4438-b7ac-2c1fc0e30db6,a1335732-8ae9-48c8-98e1-8f0aa3e2d2f2,dba58ad7-8038-4733-aa96-8c74ce16e565,2aede804-9b4f-4e24-97f1-ac7a5ba61452,3542da35-efe4-4128-a04f-07cff9a13a7c,164902bd-8c23-4ae9-b030-15c305795bb5,e19e76d2-0bcb-4993-969e-659c6840c39f", headers=headers)
        self.assertEqual(response.status_code, 200)


    def test_export_purchase_orders_to_excel(self):
        """
            Test xuất file excel đơn đặt hàng 
        """
        headers = {
            'user-token' : Config.ACCESS_TOKEN
        }
        response = requests.get(f"{Config.IVT_DOMAIN}/api/v1/excel/export-item-po?supplier_id=NCC0250&from_date=1635958800&to_date=1645721999", headers=headers)
        self.assertEqual(response.status_code, 200)


    def test_export_purchase_orders_to_excel_without_access_token(self):
        """
            Test xuất file excel đơn đặt hàng mà không có khóa truy cập
        """
        response = requests.get(f"{Config.IVT_DOMAIN}/api/v1/excel/export-item-po?supplier_id=NCC0250&from_date=1635958800&to_date=1645721999")
        self.assertEqual(response.status_code, 401)

    def test_import_excel_locked_item(self):
        """
            Test import excel đơn đặt hàng với mã hàng đã bị khóa đặt hàng 
        """

        headers = {"user-token": Config.ACCESS_TOKEN}
        with open(
            "./asset/test_import_po_error_locked_item.xlsx", "rb"
        ) as excel_file:
            response = requests.post(
                f"{Config.IVT_DOMAIN}/api/v1/services/purchase-order?import_file=true&warehouse_id=EZD081YGKKMG&province_uid=c7648902-3b2a-40f4-a891-f04682d1099d",
                files={"excel": excel_file},
                headers=headers,
            )
            self.assertEqual(response.status_code, 520)


    def test_import_excel_purchase_order(self):
        """
            Test import excel đơn đặt hàng 
        """

        headers = {"user-token": Config.ACCESS_TOKEN}
        with open(
            "./asset/test_import_po.xlsx", "rb"
        ) as excel_file:
            response = requests.post(
                f"{Config.IVT_DOMAIN}/api/v1/services/purchase-order?import_file=true&warehouse_id=EZD081YGKKMG&province_uid=c7648902-3b2a-40f4-a891-f04682d1099d",
                files={"excel": excel_file},
                headers=headers,
            )
            self.assertEqual(response.status_code, 200)


    def test_import_excel_purchase_order_without_access_token(self):
        """
            Test import excel đơn đặt hàng mà không có access_token 
        """

        with open(
            "./asset/test_import_po.xlsx", "rb"
        ) as excel_file:
            response = requests.post(
                f"{Config.IVT_DOMAIN}/api/v1/services/purchase-order?import_file=true&warehouse_id=EZD081YGKKMG&province_uid=c7648902-3b2a-40f4-a891-f04682d1099d",
                files={"excel": excel_file},
            )
            self.assertEqual(response.status_code, 401) 


    def test_import_excel_negative_item_quantity_purchase_order(self):
        """
            Test import excel đơn đặt hàng với số lượng âm 
        """

        headers = {"user-token": Config.ACCESS_TOKEN}
        with open(
            "./asset/test_import_po_negative_item_quantity.xlsx", "rb"
        ) as excel_file:
            response = requests.post(
                f"{Config.IVT_DOMAIN}/api/v1/services/purchase-order?import_file=true&warehouse_id=EZD081YGKKMG&province_uid=c7648902-3b2a-40f4-a891-f04682d1099d",
                files={"excel": excel_file},
                headers=headers,
            )
            self.assertEqual(response.status_code, 520)
