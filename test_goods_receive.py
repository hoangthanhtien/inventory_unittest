import requests 
import unittest 
from constant import Config 
import json
import time

class TestGoodsReceive(unittest.TestCase):
    def test_get_all_success_goods_receive(self):
        """
            Test lấy tất cả thông tin các đơn nhập kho thành công
        """
        headers = {
            'user-token' : Config.ACCESS_TOKEN
        }
        current_timestamp = int(time.time())
        response = requests.get(f"{Config.IVT_DOMAIN}/api/v1/services/good-receipt?status=1&types=1&from_date=1632848400&to_date={current_timestamp}&results_per_page=10000&warehouses_uid=164902bd-8c23-4ae9-b030-15c305795bb5", headers=headers)
        self.assertEqual(response.status_code, 200)

    def test_get_all_canceled_goods_receive(self):
        """
            Test lấy tất cả thông tin các đơn nhập kho đã hủy 
        """
        headers = {
            'user-token' : Config.ACCESS_TOKEN
        }
        current_timestamp = int(time.time())
        response = requests.get(f"{Config.IVT_DOMAIN}/api/v1/services/good-receipt?status=2&types=1&from_date=1632848400&to_date={current_timestamp}&results_per_page=10000&warehouses_uid=164902bd-8c23-4ae9-b030-15c305795bb5", headers=headers)
        self.assertEqual(response.status_code, 200)

    def test_get_all_po_for_gr(self):
        """
            Test lấy thông các đơn đặt hàng đã được phê duyệt để tiến hành nhập kho
        """
        params = {
            "warehouses_uid" : Config.TEST_WAREHOUSE_UID,
            "from_date" : 1636218000,
            "to_date" : int(time.time()),
            "types" : "external",
            "status" : 5,
            "results_per_page" : 10000

        }
        url = f"{Config.IVT_DOMAIN}/api/v1/services/purchase-order"
        headers = {
            'user-token' : Config.ACCESS_TOKEN
        } 
        response = requests.get(url, params=params, headers=headers)
        self.assertEqual(response.status_code, 200)


    def test_search_items_for_creating_gr(self):
        """
            Test tìm kiếm hàng hóa để tạo đơn nhập kho điều chỉnh 
        """
        params = {
            "search" : "bia",
            "active" : 1,
            "results_per_page" : 10,
            "warehouses_uid" : Config.ALL_WAREHOUSES_UID
        }
        headers = {
            'user-token' : Config.ACCESS_TOKEN
        }
        response = requests.get(f"{Config.IVT_DOMAIN}/api/v1/catalogs/items",params=params, headers=headers)
        self.assertEqual(response.status_code, 200)
