import requests
import unittest
from constant import Config
import json
import time


class TestGoodIssue(unittest.TestCase):
    last_created_gi_uid = None

    def setUp(self):
        url = f"{Config.IVT_DOMAIN}/api/v1/services/good-issue?status=1&types=4&from_date=1632762000&to_date={int(time.time())}&results_per_page=10000&warehouses_uid=164902bd-8c23-4ae9-b030-15c305795bb5"
        headers = {"user-token": Config.ACCESS_TOKEN}
        response = requests.get(url, headers=headers)
        TestGoodIssue.last_created_gi_uid = response.json().get("data")[0].get("id")

    def test_get_all_cancel_gi(self):
        """
        Test lấy tất cả đơn xuất hủy hàng hóa
        """
        url = f"{Config.IVT_DOMAIN}/api/v1/services/good-issue?status=1&types=4&from_date=1632762000&to_date={int(time.time())}&results_per_page=10000&warehouses_uid=164902bd-8c23-4ae9-b030-15c305795bb5"

        headers = {"user-token": Config.ACCESS_TOKEN}
        response = requests.get(url, headers=headers)
        self.assertEqual(response.status_code, 200)

    def test_create_canceled_goods_issue(self):
        """
        Test tạo đơn xuất hủy hàng hóa
        """
        current_time_stamp = int(time.time())
        payload = {
            "from_warehouse_uid": "164902bd-8c23-4ae9-b030-15c305795bb5",
            "to_warehouse_uid": "164902bd-8c23-4ae9-b030-15c305795bb5",
            "gi_type": 4,
            "status": 1,
            "gi_date": current_time_stamp,
            "gi_details": [
                {
                    "quantity": 10,
                    "price": 14839,
                    "vat": 0,
                    "sub_total": 148390,
                    "item_uid": "d58dab79-670f-424e-9cab-87a045d523ee",
                    "unit_uid": "fdbea26e-ee5b-4a1f-91dd-5cacce56cb8b",
                }
            ],
            "description": "Hàng hóa quá hạn",
            "reason_text": "Xuất hủy hàng hóa",
            "action_records": {
                "time_stamp": current_time_stamp,
                "action_id": 1,
                "action_name": "Tạo đơn mới",
                "user": "buyer@amg.com",
                "type": "create",
            },
        }
        headers = {"user-token": Config.ACCESS_TOKEN}
        response = requests.post(
            f"{Config.IVT_DOMAIN}/api/v1/services/good-issue",
            json.dumps(payload),
            headers=headers,
        )
        self.assertEqual(response.status_code, 200)
        self.last_created_gi_uid = response.json().get("data").get("id")

    def test_create_calcel_gi_with_zero_quantity(self):
        """
        Test tạo đơn xuất hàng với dữ liệu lỗi (số lượng hàng = 0)
        """

        current_time_stamp = int(time.time())
        payload = {
            "from_warehouse_uid": "164902bd-8c23-4ae9-b030-15c305795bb5",
            "to_warehouse_uid": "164902bd-8c23-4ae9-b030-15c305795bb5",
            "gi_type": 4,
            "status": 1,
            "gi_date": current_time_stamp,
            "gi_details": [
                {
                    "quantity": 0,
                    "price": 14839,
                    "vat": 0,
                    "sub_total": 148390,
                    "item_uid": "d58dab79-670f-424e-9cab-87a045d523ee",
                    "unit_uid": "fdbea26e-ee5b-4a1f-91dd-5cacce56cb8b",
                }
            ],
            "description": "Hàng hóa quá hạn",
            "reason_text": "Xuất hủy hàng hóa",
            "action_records": {
                "time_stamp": current_time_stamp,
                "action_id": 1,
                "action_name": "Tạo đơn mới",
                "user": "buyer@amg.com",
                "type": "create",
            },
        }
        headers = {"user-token": Config.ACCESS_TOKEN}
        response = requests.post(
            f"{Config.IVT_DOMAIN}/api/v1/services/good-issue",
            json.dumps(payload),
            headers=headers,
        )
        self.assertEqual(response.status_code, 400)

    def test_create_calcel_gi_with_negative_quantity(self):
        """
        Test tạo đơn xuất hàng với dữ liệu lỗi (số lượng hàng âm)
        """

        current_time_stamp = int(time.time())
        payload = {
            "from_warehouse_uid": "164902bd-8c23-4ae9-b030-15c305795bb5",
            "to_warehouse_uid": "164902bd-8c23-4ae9-b030-15c305795bb5",
            "gi_type": 4,
            "status": 1,
            "gi_date": current_time_stamp,
            "gi_details": [
                {
                    "quantity": -1,
                    "price": 14839,
                    "vat": 0,
                    "sub_total": 148390,
                    "item_uid": "d58dab79-670f-424e-9cab-87a045d523ee",
                    "unit_uid": "fdbea26e-ee5b-4a1f-91dd-5cacce56cb8b",
                }
            ],
            "description": "Hàng hóa quá hạn",
            "reason_text": "Xuất hủy hàng hóa",
            "action_records": {
                "time_stamp": current_time_stamp,
                "action_id": 1,
                "action_name": "Tạo đơn mới",
                "user": "buyer@amg.com",
                "type": "create",
            },
        }
        headers = {"user-token": Config.ACCESS_TOKEN}
        response = requests.post(
            f"{Config.IVT_DOMAIN}/api/v1/services/good-issue",
            json.dumps(payload),
            headers=headers,
        )
        self.assertEqual(response.status_code, 400)

    def test_cancel_cancel_gi(self):
        """
        Test hủy đơn xuất hủy
        """
        payload = {"id": TestGoodIssue.last_created_gi_uid, "status": 2}
        headers = {"user-token": Config.ACCESS_TOKEN}
        response = requests.put(
            f"{Config.IVT_DOMAIN}/api/v1/services/good-issue?action-type=status_update",
            json.dumps(payload),
            headers=headers,
        )
        self.assertEqual(response.status_code, 200)

    def test_import_excel_cancel_gi(self):
        """
        Test import thông tin đơn xuất hủy từ file excel
        """

        headers = {"user-token": Config.ACCESS_TOKEN}
        with open("./asset/test_import_gi.xlsx", "rb") as excel_file:
            response = requests.post(
                f"{Config.IVT_DOMAIN}/api/v1/services/good-issue?import_file=true&type=GI&gi_type=4",
                files={"excel": excel_file},
                headers=headers,
            )
            self.assertEqual(response.status_code, 200)

    def test_import_missing_item_cancel_gi(self):
        """
        Test import hàng hóa không tồn tại
        """

        headers = {"user-token": Config.ACCESS_TOKEN}
        with open("./asset/test_import_gi_missing_item.xlsx", "rb") as excel_file:
            response = requests.post(
                f"{Config.IVT_DOMAIN}/api/v1/services/good-issue?import_file=true&type=GI&gi_type=4",
                files={"excel": excel_file},
                headers=headers,
            )
            self.assertEqual(response.status_code, 520)

    def test_import_invalid_item_quantity_cancel_gi(self):
        """
        Test import hàng hóa với số lượng lỗi (Kiểu string)
        """

        headers = {"user-token": Config.ACCESS_TOKEN}
        with open(
            "./asset/test_import_gi_invalid_item_quantity.xlsx", "rb"
        ) as excel_file:
            response = requests.post(
                f"{Config.IVT_DOMAIN}/api/v1/services/good-issue?import_file=true&type=GI&gi_type=4",
                files={"excel": excel_file},
                headers=headers,
            )
            self.assertEqual(response.status_code, 520)

    def test_import_negative_item_quantity_cancel_gi(self):
        """
        Test import hàng hóa với số lượng lỗi (Số lượng âm)
        """

        headers = {"user-token": Config.ACCESS_TOKEN}
        with open(
            "./asset/test_import_gi_negative_item_quantity.xlsx", "rb"
        ) as excel_file:
            response = requests.post(
                f"{Config.IVT_DOMAIN}/api/v1/services/good-issue?import_file=true&type=GI&gi_type=4",
                files={"excel": excel_file},
                headers=headers,
            )
            self.assertEqual(response.status_code, 520)

    def test_create_internal_goods_issue(self):
        """
        Test tạo đơn xuất hàng hóa nội bộ từ kho này tới kho khác 
        """
        current_time_stamp = int(time.time())
        payload =  {
            "to_warehouse_uid": "3542da35-efe4-4128-a04f-07cff9a13a7c",
            "from_warehouse_uid": "164902bd-8c23-4ae9-b030-15c305795bb5",
            "gi_type": 3,
            "status": 3,
            "gi_date": current_time_stamp,
            "gi_details": [
                {
                    "quantity": 10,
                    "price": 7082.9,
                    "vat": 0,
                    "sub_total": 70829,
                    "item_uid": "0c307d10-93f4-496a-b50f-7588fe85680d",
                    "unit_uid": "b4ef2ea5-f70f-4fdd-a219-372e0c7667d5"
                }
            ],
            "action_records": {
                "time_stamp": current_time_stamp,
                "action_id": 3,
                "action_name": "Tạo đơn mới",
                "user": "buyer@amg.com",
                "type": "create"
            }
        }     
        headers = {"user-token": Config.ACCESS_TOKEN}
        response = requests.post(
            f"{Config.IVT_DOMAIN}/api/v1/services/good-issue",
            json.dumps(payload),
            headers=headers,
        )
        self.assertEqual(response.status_code, 200)

    def test_accept_internal_goods_issue(self):
        """
        Test xác nhận đơn xuất hàng hóa nội bộ từ kho này tới kho khác 
        """
        payload = {"id": TestGoodIssue.last_created_gi_uid, "status": 1}
        headers = {"user-token": Config.ACCESS_TOKEN}
        response = requests.put(
            f"{Config.IVT_DOMAIN}/api/v1/services/good-issue?action-type=status_update",
            json.dumps(payload),
            headers=headers,
        )
        breakpoint()
        self.assertEqual(response.status_code, 200)
