# Inventory Unitest

## Login (done) - Đăng nhập  
| Steps  | Data   | Expected Result |Actual Result|Status|Comment|
|-------------- | -------------- | -------------- |------|------|-----|
| Test login với tài khoản đã được đăng ký| user_email/user_password | HTTP status 200| HTTP status 200 | pass | |
| Test login thất bại với tài khoản chưa được đăng ký | dummy_account@gmail.com/123456| HTTP status 401| HTTP status 401 | pass | |
|  Test login với tài khoản không hợp lệ  | this_is_not_an_email/12| HTTP status 401| HTTP status 401 | pass | |


## Item - Quản lý hàng hóa 
| Steps  | Data   | Expected Result |Actual Result|Status|Comment|
|-------------- | -------------- | -------------- |------|------|-----|
| Test thêm mới hàng hóa trùng tên | item_name="Dừa xiêm bến tre loại 1" | HTTP status code != 200 | HTTP status code != 200 | pass| |
| Test thêm mới hàng hóa | {'item_name':'New Item','unit_uid': 'da0cc896-18ff-48c9-bae3-a8aa84191eeb'} | HTTP status 200 | HTTP status 200 | pass | |
| Test thêm mới hàng hóa với tên hàng hóa rỗng | {'item_name':'','unit_uid': 'da0cc896-18ff-48c9-bae3-a8aa84191eeb'} | HTTP status 400 | HTTP status 200 | fail | |
|  Tạo mới hàng hóa mà không có đơn vị tính  | {'item_name':'test add item','unit_uid': ''} | HTTP status 400 | HTTP status 400 | pass | |
| Test lấy danh sách hàng hóa  | user-token | HTTP status 200 | HTTP status 200 | pass | |
| Test lấy danh sách hàng hóa theo phần trang| {'page': 2,'item_class_uid': ''} | HTTP status 200 | HTTP status 200 | pass | |
| Test lấy danh sách hàng hóa theo nhóm hàng | "item_class_uid": "0630e497-9403-42d5-92f9-7136f5fe171a" | HTTP status 200 | HTTP status 200 | pass | |
| Test lấy thông tin hàng hóa theo id | "item_uid": "8c036c35-4dcf-46a0-87cc-c286db2d2fe2" | HTTP status 200 | HTTP status 500 | fail | Lỗi server |
| Test cập nhật thông tin hàng hóa | Thông tin hàng hóa update | HTTP status 200 | HTTP status 200 | pass | |
| Test xóa hàng hóa theo id | id mã hàng cần xóa | HTTP status 200 | HTTP status 200 | pass | |

- Steps: Test delete item
    + data: 
    + Expected Result: 
    + Actual Result: 
    + Status: pass
    + comment: 
***
- Steps: Test search items
    + data: 
    + Expected Result: 
    + Actual Result: 
    + Status: pass
    + comment: 
***
- Steps: Test upload items
    + data: 
    + Expected Result: 
    + Actual Result: 
    + Status: pass
    + comment: 
## Purchase order - Quản lý đơn đặt hàng  
| Steps  | Data   | Expected Result |Actual Result|Status|Comment|
|-------------- | -------------- | -------------- |------|------|-----|
| Test lấy tất cả thông tin các đơn nhập kho thành công | status = 1 | HTTP status 200| HTTP status 200 | pass | |
| Test lấy tất cả thông tin các đơn nhập kho đang chờ | status = 3 | HTTP status 200| HTTP status 200 | pass | |
| Test lấy tất cả thông tin các đơn nhập kho đã hủy | status = 2 | HTTP status 200| HTTP status 200 | pass | |
| Test lấy tất cả thông tin các đơn nhập kho đã được duyệt | status = 5 | HTTP status 200| HTTP status 200 | pass | |
| Test tìm kiếm tất cả đơn đặt hàng theo mã hàng | item_id=00000088 | HTTP status 200| HTTP status 200 | pass | |
| Test xuất file excel đơn đặt hàng | user-token | HTTP status 200| HTTP status 200 | pass | |
| Test xuất file excel đơn đặt hàng mà không có khóa truy cập | | HTTP status 401| HTTP status 401| pass | |
| Test import excel đơn đặt hàng với mã hàng đã bị khóa đặt hàng |./asset/test_import_po_error_locked_item.xlsx | HTTP status 520| HTTP status 520| pass | |
| Test import excel đơn đặt hàng | ./asset/test_import_po.xlsx| HTTP status 200| HTTP status 200 | pass | |
| Test import excel đơn đặt hàng mà không có access token | ./asset/test_import_po.xlsx| HTTP status 401| HTTP status 401 | pass | |
| Test import excel đơn đặt hàng với số lượng mặt hàng âm | ./asset/test_import_po_negative_item_quantity.xlsx| HTTP status 520 | HTTP status 200 | fail | Vẫn có thể import số lượng hàng hóa âm vào đơn đặt hàng |
## Good receipt - Quản lý nhập hàng 
| Steps  | Data   | Expected Result |Actual Result|Status|Comment|
|-------------- | -------------- | -------------- |------|------|-----|
| Test lấy tất cả thông tin các đơn nhập kho thành công | user-token | HTTP status 200| HTTP status 200 | pass | |
| Test lấy tất cả thông tin các đơn nhập kho đã hủy | user-token | HTTP status 200| HTTP status 200 | pass | |
| Test lấy thông các đơn đặt hàng đã được phê duyệt để tiến hành nhập kho | /api/v1/services/purchase-order, status = 5 | HTTP status 200| HTTP status 200 | pass | Bổ sung |
| Test tìm kiếm hàng hóa để tạo đơn nhập kho điều chỉnh | /api/v1/catalogs/items | HTTP status 200| HTTP status 200 | pass | Bổ sung |

## Good issue - Quản lý xuất hàng  

| Steps  | Data   | Expected Result |Actual Result|Status|Comment|
|-------------- | -------------- | -------------- |------|------|-----|
| Test lấy tất cả đơn xuất hủy hàng hóa | user-token | HTTP status 200| HTTP status 200 | pass | |
| Test tạo đơn xuất hủy hàng hóa | Thông tin đơn xuất hủy + user-token | HTTP status 200 | HTTP status 200 | pass | |
| Test hủy đơn xuất hủy | Thông tin xuất hủy cần được hủy + user-token | HTTP status 200 | HTTP status 200 | pass | |
| Test tạo đơn xuất hàng với dữ liệu lỗi (số lượng hàng = 0) | Thông tin xuất hủy cần được hủy với số lượng xuất = 0 + user-token | HTTP status 400 | HTTP status 400| pass | |
| Test tạo đơn xuất hàng với dữ liệu lỗi (số lượng hàng là số âm) | Thông tin xuất hủy cần được hủy với số lượng xuất = -1 + user-token | HTTP status 400 | HTTP status 200| fail | Số lượng hàng hóa là số âm vẫn có thể tạo phiếu xuất |
| Test import thông tin đơn xuất hủy từ file excel | asset/test_import_gi.xlsx | HTTP status 200 | HTTP status 200| pass | |
| Test import hàng hóa không tồn tại từ file excel |./asset/test_import_gi_missing_item.xlsx| HTTP status 520 | HTTP status 520| pass | |
| Test import hàng hóa với số lượng lỗi (Kiểu string)|./asset/test_import_gi_invalid_item_quantity.xlsx| HTTP status 520 | HTTP status 200| fail | Vẫn có thể import số lượng kiểu string |
| Test import hàng hóa với số lượng lỗi (Số lượng âm)|./asset/test_import_gi_negative_item_quantity.xlsx| HTTP status 520 | HTTP status 200| fail | Vẫn có thể import số lượng âm |
| Test tạo đơn xuất hàng hóa nội bộ từ kho này tới kho khác |Thông tin phiêu xuất kho nội bộ| HTTP status 200 | HTTP status 200| pass |  |
| Test xác nhận đơn xuất hàng hóa nội bộ từ kho này tới kho khác|GI status = 1| HTTP status 200 | HTTP status 520| fail | Lỗi test, sẽ check lại sau |
